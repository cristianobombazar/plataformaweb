app.controller("relatorioPedidoController", function($scope, $rootScope, $http, store, $location, mensagens, $mdDialog, $routeParams, $route, consultas) {

    $scope.filtro ={
      'dataInicial': dataSemHora(),
      'dataFinal': dataSemHora(),
      'status' : null,
       produto: null,
       cliente: null
    };

    $scope.status = [
        {'id': null,
            'descricao': 'Todos'},
        {'id': 1,
         'descricao': 'Pendente'},
        {'id': 2,
            'descricao': 'Liberado'},
        {'id': 3,
            'descricao': 'Faturado'},
        {'id': 4,
            'descricao': 'Cancelado'}
    ];

    $scope.emitir = function(filtro){
        if (filtro.dataInicial && filtro.dataFinal){
            $http.post('/sys/relatorio/faturamento/relatorioPedido', filtro, {
                responseType: 'arraybuffer'
            }).then(function(response) {
                if (response.status === 204){
                    mensagens.showAlert("Sem dados para exibição", 'info', '', 3000);
                }else{
                    var blob = new Blob([response.data], {
                        type: ('application/pdf')
                    });
                    saveAs(blob, 'RelatorioPedido.pdf');
                }
            });
        }
    };

    $scope.limpar = function(){
        $scope.filtro ={
            'dataInicial': dataSemHora(),
            'dataFinal':   dataSemHora(),
            'status' : null
        };
    };

    $scope.dialogProduto = function(event) {
        consultas.produto(event, function(produto) {
            $scope.filtro.produto = produto;
        });
    };

    $scope.dialogCliente = function(event) {
        consultas.cliente(event, function(cliente) {
            $scope.filtro.cliente = cliente;
        });
    };

});