app.controller("relatorioProdutoVendidoController", function($scope, $rootScope, $http, store, $location, mensagens, $mdDialog, $routeParams, $route) {

    $scope.filtro ={
      'dataInicial': dataSemHora(),
      'dataFinal': dataSemHora()
    };

    $scope.emitir = function(filtro){
        if (filtro.dataInicial && filtro.dataFinal){
            $http.post('/sys/relatorio/faturamento/relatorioProdutoVendido', filtro, {
                responseType: 'arraybuffer'
            }).then(function(response) {
                if (response.status === 204){
                    mensagens.showAlert("Sem dados para exibição", 'info', '', 3000);
                }else{
                    var blob = new Blob([response.data], {
                        type: ('application/pdf')
                    });
                    saveAs(blob, 'RelatorioProdutoVendido.pdf');
                }
            });
        }
    };

    $scope.limpar = function(){
        $scope.filtro ={
            'dataInicial': dataSemHora(),
            'dataFinal':   dataSemHora()
        };
    };

});