app.controller("produtoController", function($scope, $rootScope, $http, store, $location, mensagens, $mdDialog, $routeParams, $route) {

    $scope.init = function(){
        $http.get("/sys/cadastro/produto")
        .then(function(response) {
            $scope.listaProduto = response.data;
        }, function(response) {
            mensagens.showDataAlert(response.data);
        });
    };

    $scope.initEdit = function(){
        if ($scope.produtoForm.$valid) { //validação Form
            if ($routeParams.id){
                $http.get("/sys/cadastro/produto/"+$routeParams.id)
                .then(function(response) {
                    $scope.produto = response.data;
                }, function(response) {
                    mensagens.showDataAlert(response.data);
                });
            }else{
                $scope.produto = {
                    supermercado: {}
                };
            }
        }
    };

    $scope.salvar = function(produto){
        if ($scope.produtoForm.$valid) { //validação Form
            var usuario = $rootScope.usuario;
            if (usuario && usuario.supermercado){
              produto.supermercado = usuario.supermercado;
            }
            $http.post("/sys/cadastro/produto", produto)
            .then(function(response) {
                if (response.data){
                    mensagens.showAlert('Produto salvo com sucesso!', 'success', '', 5000);
                }
                if (produto.id){
                    $scope.voltar();    
                }else{
                    $route.reload();
                }
            }, function(response) {
                mensagens.showDataAlert(response.data);
            });
        }
    };

    $scope.excluir = function(produto){
        $mdDialog.show($mdDialog.confirm()
        .textContent('Confirmar exclusão do produto ' + produto.descricao + '?')
        .ok('SIM').cancel('NÃO')).then(function() {
            $http.delete("/sys/cadastro/produto/" + produto.id)
            .then(function(response) {
                mensagens.showAlert('Produto removido com sucesso!', 'success', '', 5000);
                $scope.init();
            }, function(response) {
                mensagens.showDataAlert(response.data);
            });
        });
    };

    $scope.geraQrCode = function(){
        var data = new Date();
        $scope.produto.generatedValueQrCodeEncoder = data.getTime();
        $http.get("/sys/cadastro/produto/generateQrCodeProduto",  {
              params: {
                'generatedValue' : $scope.produto.generatedValueQrCodeEncoder,
                'descricao':       $scope.produto.descricao
              }
          }).then(function(response) {
              var result = response.data.qrCode;
              $scope.produto.qrCode = response.data.qrCode;
            //   var file = new File(result);
            //   $scope.encodeImageFileAsURL(file);
          }, function(response) {
              mensagens.showDataAlert(response.data);
          });
    }

    $scope.encodeImageFileAsURL = function(element){            
         var file = element.files[0];
        var reader = new FileReader();
        reader.onloadend = function() {
            $scope.produto.imagem = reader.result;    
        };
        reader.readAsDataURL(file);
    };

    $scope.voltar = function(){
        $location.path("consultaProduto");
    };

});
