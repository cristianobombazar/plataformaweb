app.controller("clienteController", function($scope, $rootScope, $http, store, $location, mensagens, $mdDialog, $routeParams, $route) {

    $scope.tipoCartao = [

        {id: 1,
            descricao: 'DEBITO',
            controle: 'VISA'
        },
        {id: 2,
            descricao: 'CRÉDITO',
            controle: 'CREDITO'
        },
        {id: 3,
            descricao: 'ALIMENTAÇÃO',
            controle: 'ALIMENTACAO'
        },
        {id: 4,
            descricao: 'OUTRO',
            controle: 'OUTRO'
        }
    ];

    $scope.bandeiraCartao = [

        {id: 1,
            descricao: 'VISA',
            controle: 'VISA'
        },
        {id: 2,
            descricao: 'MASTERCARD',
            controle:  'MASTERCARD'
        },
        {id: 3,
            descricao: 'ELO',
            controle:  'ELO'
        },
        {id: 4,
            descricao: 'AMERICAN EXPRESS',
            controle:  'AMERICAN_EXPRESS'
        },
        {id: 5,
            descricao: 'DISCOVERY NETWORK',
            controle:  'DISCOVERY_NETWORK'
        },
        {id: 6,
            descricao: 'OUTRO',
            controle:  'OUTRO'
        }
    ];

    $scope.init = function(){
        $http.get("/sys/cadastro/cliente")
        .then(function(response) {
            $scope.listaClientes = response.data;
        }, function(response) {
            mensagens.showDataAlert(response.data);
        });
    };

    $scope.initEdit = function(){
        if ($scope.clienteForm.$valid) { //validação Form
            $scope.cartoesExcluidos = [];
            if ($routeParams.id){
                $http.get("/sys/cadastro/cliente/"+$routeParams.id)
                .then(function(response) {
                    $scope.cliente = response.data;
                }, function(response) {
                    mensagens.showDataAlert(response.data);
                });
            }else{
                $scope.cliente = {
                    'indicadorUso' : 1,
                    'tipo': 1,
                    'endereco': {},
                    'cartoes' : []
                };
            }
        }
    };

    $scope.salvar = function(cliente){
        if ($scope.clienteForm.$valid) { //validação Form
            $http.post("/sys/cadastro/cliente", cliente)
            .then(function(response) {
                if (response.data){
                    mensagens.showAlert('Cliente salvo com sucesso!', 'success', '', 5000);
                    $scope.deletarCartoes();
                }
                if (cliente.id){
                    $scope.voltar();    
                }else{
                    $route.reload();
                }                       
            }, function(response) {
                mensagens.showDataAlert(response.data);
            });
       }
    };

/*    $scope.submitForm = function() {
        // verifica se o formulário é válido
        
    };*/

    $scope.excluir = function(cliente){
        $mdDialog.show($mdDialog.confirm()
        .textContent('Confirmar exclusão do cliente ' + cliente.nome + '?')
        .ok('SIM').cancel('NÃO')).then(function() {
            $http.delete("/sys/cadastro/cliente/" + cliente.id)
            .then(function(response) {
                mensagens.showAlert('Cliente removido com sucesso!', 'success', '', 5000);
                $scope.init();
            }, function(response) {
                mensagens.showDataAlert(response.data);
            });
        });
    }

    $scope.deletarCartoes = function(){
        if ($scope.cartoesExcluidos && $scope.cartoesExcluidos.length > 0){
            $scope.cartoesExcluidos.forEach(function(cartao){
                $http.delete("/sys/cadastro/cliente/deletarCartao/" + cartao.id)
                .then(function(response) {
                    console.log('DELETANDO CARTÃO: '+cartao.numeroCartao+'. CLIENTE: '+cartao.nomeImpresso);
                }, function(response) {
                    mensagens.showDataAlert(response.data);
                });
            });
        }
    };

    $scope.editarCartao = function(cartao){
        $scope.cartao = angular.copy(cartao);
    };

    $scope.excluirCartao = function(cartao){
        if (cartao.id){
            $scope.cartoesExcluidos.push(cartao);
        }
        $scope.cliente.cartoes.splice($scope.cliente.cartoes.indexOf(cartao), 1);
        //$scope.cartao = cartao;
    }

    $scope.adicionarCartao = function(cartao){
        $scope.cliente.cartoes.push(cartao);
        $scope.cartao = {};
    };
    $scope.voltar = function(){
        $location.path("consultaCliente");
    };

});
