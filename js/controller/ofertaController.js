app.controller("ofertaController", function($scope, $rootScope, $http, store, $location, mensagens, $mdDialog, $routeParams, consultas, $route) {

    $scope.init = function(){
        $http.get("/sys/faturamento/oferta")
        .then(function(response) {
            $scope.listaOfertas = response.data;
        }, function(response) {
            mensagens.showDataAlert(response.data);
        });
    };

    $scope.initEdit = function(){
        if ($scope.ofertaForm.$valid) { //validação Form
            $scope.item = {
                'produto': null
            };
            $scope.oferta = {
                // 'dataCadastro': dataSemHora(),
                // 'descricao': 'teste cristiano',
                // 'dataInicio': dataSemHora(),
                // 'dataFim': dataSemHora(),
                // 'obs': 'teste cristiano',
                'produtos': []
            };
            $scope.itemExcluidos = [];

            if ($routeParams.id){
                $http.get("/sys/faturamento/oferta/"+$routeParams.id)
                    .then(function(response) {
                        $scope.oferta = response.data;
                    }, function(response) {
                        mensagens.showDataAlert(response.data);
                    });
            }
        }
    };

    $scope.deletarItem = function(){
      if ($scope.itemExcluidos && $scope.itemExcluidos.length > 0){
        $scope.itemExcluidos.forEach(function(item){
            $http.delete("/sys/faturamento/oferta/item/" + item.id)
                .then(function(response) {
                    console.log('DELETANDO ITEM: '+item.id);
                }, function(response) {
                    mensagens.showDataAlert(response.data);
                });
        });
      }
    };

    $scope.dialogProduto = function(event) {
        consultas.produto(event, function(produto) {
            $scope.item.produto = produto;
        });
    };


    $scope.salvar = function(oferta){
        if ($scope.ofertaForm.$valid) { //validação Form
            $http.post("/sys/faturamento/oferta", oferta)
            .then(function(response) {
                if (response.data){
                    mensagens.showAlert('Oferta salva com sucesso!', 'success', '', 5000);
                    $scope.deletarItem();
                }   
                if (oferta.id){
                    $scope.voltar();    
                }else{
                    $route.reload();
                }                     
            }, function(response) {
                mensagens.showDataAlert(response.data);
            });
        }
    };

    // $scope.excluir = function(oferta){
    //     $mdDialog.show($mdDialog.confirm()
    //     .textContent('Confirmar exclusão da oferta ' + oferta.descricao + '?')
    //     .ok('SIM').cancel('NÃO')).then(function() {
    //         $http.delete("/sys/faturamento/oferta/" + oferta.id)
    //         .then(function(response) {
    //             mensagens.showAlert('Oferta removida com sucesso!', 'success', '', 5000);
    //             $scope.init();
    //         }, function(response) {
    //             mensagens.showDataAlert(response.data);
    //         });
    //     });
    // };

    $scope.editarItem = function(item){
        $scope.item  = angular.copy(item);
    };

    $scope.excluirItem = function(item){
        if (item.id){
            $scope.itemExcluidos.push(item);
        }
        $scope.oferta.produtos.splice($scope.oferta.produtos.indexOf(item), 1);
    }

    $scope.adicionarItem = function(itemNovo){
        var existeProduto = false;
        var index = 0;
        if (validaItem(itemNovo)){
            if ($scope.oferta.produtos && $scope.oferta.produtos.length > 0){
                for (var i = 0; i < $scope.oferta.produtos.length; i++) {
                    var item = $scope.oferta.produtos[i];
                    if (itemNovo.produto.id === item.produto.id){
                        existeProduto = true;
                        index = i;
                        break;
                    }
                }
            }
            if (!existeProduto){
                $scope.oferta.produtos.push(itemNovo);
            }else{
                $scope.oferta.produtos[index] = itemNovo;
            }
            $scope.item = {
                'produto': null
            };
        }else{
            mensagens.showAlert("Favor, preencha todos os campos para incluir um produto na oferta!", 'info', '', 3000);
        }

    };

    function validaItem(item){
        if (!item){
            return false;
        }
        if (!item.produto){
            return false;
        }
        if (!item.quantidade){
            return false;
        }
        if (!item.quantidadeDisponivel){
            return false;
        }
        if (!item.valor){
            return false;
        }
        return true;
    }

    $scope.voltar = function(){
        $location.path("consultaOferta");
    };


});
