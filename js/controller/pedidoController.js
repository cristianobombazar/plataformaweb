app.controller("pedidoController", function($scope, $rootScope, $http, store, $location, mensagens, $mdDialog, $routeParams, consultas, $route) {

    $scope.descSituacao = {
        1: "Pendente",
        2: "Liberado",
        3: "Faturado",
        4: "Cancelado"
    };

    $scope.init = function(){
        $http.get("/sys/faturamento/pedido")
        .then(function(response) {
            $scope.listaPedido = response.data;
        }, function(response) {
            mensagens.showDataAlert(response.data);
        });

        $http.get("/sys/cadastro/formaPagamento")
            .then(function(response) {
                $scope.listaFormas = response.data;
            }, function(response) {
                mensagens.showDataAlert(response.data);
         });
    };

    $scope.initEdit = function(){
        $scope.item = {
            'produto': null
        };
        $scope.pedido = {
            'data': dataSemHora(),
            'cliente':{},
            'itens': []
        };
        $scope.itemExcluidos = [];

        if ($routeParams.id){
            $http.get("/sys/faturamento/pedido/"+$routeParams.id)
                .then(function(response) {
                    $scope.pedido = response.data;
                }, function(response) {
                    mensagens.showDataAlert(response.data);
                });
        }
        $http.get("/sys/cadastro/formaPagamento")
            .then(function(response) {
                $scope.listaFormas = response.data;
            }, function(response) {
                mensagens.showDataAlert(response.data);
         });
    };

    $scope.deletarItem = function(){
      if ($scope.itemExcluidos && $scope.itemExcluidos.length > 0){
        $scope.itemExcluidos.forEach(function(item){
            $http.delete("/sys/faturamento/pedido/item/" + item.id)
                .then(function(response) {
                    console.log('DELETANDO ITEM: '+item.id);
                }, function(response) {
                    mensagens.showDataAlert(response.data);
                });
        });
      }
    };

    $scope.dialogProduto = function(event) {
        consultas.produto(event, function(produto) {
            $scope.item.produto = produto;
        });
    };

    $scope.dialogCliente = function(event) {
        consultas.cliente(event, function(cliente) {
            $scope.pedido.cliente = cliente;
        });
    };

    $scope.dialogOferta = function(event) {
        consultas.oferta(event, function(oferta) {
            $scope.pedido.oferta = oferta;
        });
    };


    $scope.salvar = function(oferta){
        $http.post("/sys/faturamento/pedido", oferta)
        .then(function(response) {
            if (response.data){
                $scope.produto = response.data;
                mensagens.showAlert('Pedido salva com sucesso!', 'success', '', 5000);
                $scope.deletarItem();
            }
            if (oferta.id){
                $scope.voltar();    
            }else{
                $route.reload();
            }
        }, function(response) {
            mensagens.showDataAlert(response.data);
        });
    };

    $scope.excluir = function(pedido){
        $mdDialog.show($mdDialog.confirm()
        .textContent('Confirmar exclusão do Pedido ' + pedido.id + '?')
        .ok('SIM').cancel('NÃO')).then(function() {
            $http.delete("/sys/faturamento/pedido/" + pedido.id)
            .then(function(response) {
                mensagens.showAlert('Pedido removido com sucesso!', 'success', '', 5000);
                $scope.init();
            }, function(response) {
                mensagens.showDataAlert(response.data);
            });
        });
    };

    $scope.editarItem = function(item){
        $scope.item  = angular.copy(item);
    };

    $scope.excluirItem = function(item){
        if (item.id){
            $scope.itemExcluidos.push(item);
        }
        $scope.pedido.itens.splice($scope.pedido.itens.indexOf(item), 1);
    }

    $scope.adicionarItem = function(itemNovo){
        var existeProduto = false;
        var index = 0;
        if (validaItem(itemNovo)){
            if ($scope.pedido.itens && $scope.pedido.itens.length > 0){
                for (var i = 0; i < $scope.pedido.itens.length; i++) {
                    var item = $scope.pedido.itens[i];
                    if (itemNovo.produto.id === item.produto.id){
                        existeProduto = true;
                        index = i;
                        break;
                    }
                }
            }
            if (!existeProduto){
                $scope.pedido.itens.push(itemNovo);
            }else{
                $scope.pedido.itens[index] = itemNovo;
            }
            $scope.item = {
                'produto': null
            };
        }else{
            mensagens.showAlert("Favor, preencha todos os campos para incluir um produto no pedido!", 'info', '', 3000);
        }

    };

    function validaItem(item){
        if (!item){
            return false;
        }
        if (!item.produto){
            return false;
        }
        if (!item.quantidade){
            return false;
        }
        if (!item.valor){
            return false;
        }
        return true;
    }

    $scope.voltar = function(){
        $location.path("consultaPedido");
    };


});
