app.controller('indexController', function($http, $rootScope, $scope, $cookies) {
        var usuario = $cookies.getObject('usuario');
        $cookies.remove('usuario');
        if (usuario && usuario.login){
        $http.get("/sys/cadastro/usuario/findUserByLogin",  {
              params: {
                'login' : usuario.login
              }
          }).then(function(response) {
              $rootScope.usuario = response.data;
          }, function(response) {
              mensagens.showDataAlert(response.data);
          });
        }
});
