app.controller("supermercadoController", function($scope, $rootScope, $http, store, $location, mensagens, $mdDialog, $routeParams, consultas, $route) {
    //app.controller("supermercadoController", function($scope, $http, consultas, $filter, mensagens, $timeout, $mdDialog, $routeParams, $uibModal) {
    $scope.status = ['Ativo', 'Inativo'];
    
    $scope.init = function(){
        $scope.supermercado = {
            'indicadorUso' : 1,
            'endereco': {}
        };
        $http.get("/sys/cadastro/supermercado")
        .then(function(response) {
            $scope.listaSupermercado = response.data;
        }, function(response) {
            mensagens.showDataAlert(response.data);
        });
    };
    $scope.initEdit = function(){
        if ($routeParams.id){
            $http.get("/sys/cadastro/supermercado/"+$routeParams.id)
            .then(function(response) {
                $scope.supermercado = response.data;
            }, function(response) {
                mensagens.showDataAlert(response.data);
            });
        }else{
            $scope.supermercado = {
                'indicadorUso' : 1,
                'endereco': {}
            };
        }
    };
    $scope.salvar = function(supermercado){
        if ($scope.supermercadoForm.$valid) {
            $http.post("/sys/cadastro/supermercado", supermercado)
            .then(function(response) {
                if (response.data){
                    mensagens.showAlert('Supermercado salvo com sucesso!', 'success', '', 5000);
                }
                if (supermercado.id){
                    $scope.voltar();    
                }else{
                    $route.reload();
                }
            }, function(response) {
                mensagens.showDataAlert(response.data);
            });
        }
    };

    $scope.editar = function(supermercado){
        if ($scope.supermercadoForm.$valid) {
            $scope.supermercado = {
            'indicadorUso' : 1,
            };
            if ($routeParams.id){
                $http.get("/sys/cadastro/supermercado/"+$routeParams.id)
                .then(function(response) {
                    $scope.supermercado = response.data;
                }, function(response) {
                    mensagens.showDataAlert(response.data);
                });
            }
        }
    }

    $scope.excluir = function(supermercado){
        $mdDialog.show($mdDialog.confirm()
        .textContent('Confirmar exclusão do supemercado ' + supermercado.razaoSocial + '?')
        .ok('SIM').cancel('NÃO')).then(function() {
            $http.delete("/sys/cadastro/supermercado/" + supermercado.id)
            .then(function(response) {
                mensagens.showAlert('Supermercado removido com sucesso!', 'success', '', 5000);
                $scope.init();
            }, function(response) {
                mensagens.showDataAlert(response.data);
            });
        });
    };

    $scope.encodeImageFileAsURL = function(element){
        var file = element.files[0];
        var reader = new FileReader();
        reader.onloadend = function() {
            $scope.supermercado.logo = reader.result;
        };
        reader.readAsDataURL(file);
    };

    $scope.limpar = function(){
        $scope.supermercado = {
            'indicadorUso' : 1,
            'endereco': {}
        };
    };

    $scope.voltar = function(){
        $location.path("consultaSupermercado");
    };

});
