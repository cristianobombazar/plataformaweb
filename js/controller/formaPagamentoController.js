app.controller("formaPagamentoController", function($scope, $rootScope, $http, store, $location, mensagens, $mdDialog, $routeParams, $route) {

    $scope.status = ['Ativo', 'Inativo'];

    $scope.init = function(){
        $http.get("/sys/cadastro/formaPagamento")
        .then(function(response) {
            $scope.listaFormas = response.data;
        }, function(response) {
            mensagens.showDataAlert(response.data);
        });
    };

    $scope.initEdit = function(){
        $scope.formaPagamento = {
            'indicadorUso' : 1,
        };
        if ($routeParams.id){
            $http.get("/sys/cadastro/formaPagamento/"+$routeParams.id)
            .then(function(response) {
                $scope.formaPagamento = response.data;
            }, function(response) {
                mensagens.showDataAlert(response.data);
            });
        }
    };

    $scope.salvar = function(formaPagamento){
        $http.post("/sys/cadastro/formaPagamento", formaPagamento)
        .then(function(response) {
            if (response.data){
                mensagens.showAlert('Forma de Pagamento salvo com sucesso!', 'success', '', 5000);
            }
            if (formaPagamento.id){
                $scope.voltar();    
            }else{
                $route.reload();
            }              
        }, function(response) {
            mensagens.showDataAlert(response.data);
        });
    };

    $scope.excluir = function(formaPagamento){
        $mdDialog.show($mdDialog.confirm()
        .textContent('Confirmar exclusão da forma de pagamento ' + formaPagamento.descricao + '?')
        .ok('SIM').cancel('NÃO')).then(function() {
            $http.delete("/sys/cadastro/formaPagamento/" + formaPagamento.id)
            .then(function(response) {
                mensagens.showAlert('Forma de Pagamento removido com sucesso!', 'success', '', 5000);
                $scope.init();
            }, function(response) {
                mensagens.showDataAlert(response.data);
            });
        });
    };
    $scope.voltar = function(){
        $location.path("consultaFormaPagamento");
    };

});
