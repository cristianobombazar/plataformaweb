
var regexIso8601 = /^(\d{4}|\+\d{6})(?:-(\d{2})(?:-(\d{2})(?:T(\d{2}):(\d{2})(Z|([\-+])(\d{2}):(\d{2}))?)?)?)?$/;
var regexIso8601 = /^(\d{4}|\+\d{6})-(\d{2})-(\d{2})(?:T(\d{2}):(\d{2})(:\d{2}.\d{3})?(Z|(([\-+])(\d{2})(:?(\d{2}))?)?)?)?$/;

function convertDateStringsToDates(input) {
    if (typeof input !== "object") return input;

    for (var key in input) {
        if (!input.hasOwnProperty(key)) continue;

        var value = input[key];
        var match;
        if (typeof value === "string" && (match = value.match(regexIso8601))) {
            input[key] = moment(match[0]).toDate();
        } else if (typeof value === "object") {
            convertDateStringsToDates(value);
        }
    }
}

function convertDatesToDateStrings(input, $filter) {
    if (typeof input !== "object") return input;

    for (var key in input) {
        if (!input.hasOwnProperty(key)) continue;
        var value = input[key];
        var match;
        if(value instanceof Date){
            input[key] = $filter('date')(value, 'yyyy-MM-ddTHH:mmZ');
        } else if (typeof value === "object") {
            convertDatesToDateStrings(value);
        }
    }
}

function dataHora() {
    var dh = new Date();
    dh.setSeconds(0);
    dh.setMilliseconds(0);
    return dh;
}

function dataSemHora() {
    var dh = new Date();
    dh.setHours(0, 0, 0, 0, 0);
    return dh;
}

function contem(str, substr) {
    if (str && substr) {
        return str.toUpperCase().indexOf(substr.toUpperCase()) >= 0;
    }
    return true;
}

function formatPriciseDiff(value) {
    value = replace(value, "months", "meses");
    value = replace(value, "month", "mês");
    value = replace(value, "days", "dias");
    value = replace(value, "day", "dia");
    value = replace(value, "hours", "horas");
    value = replace(value, "hour", "hora");
    value = replace(value, "minutes", "minutos");
    value = replace(value, "minute", "minuto");
    value = replace(value, "seconds", "segundos");
    value = replace(value, "second", "segundo");
    return value;
}

function replace(value, oldValue, newValue) {
    if (value && oldValue) {
        if (contem(value, oldValue)) {
            return value.replace(oldValue, newValue);
        } else {
            return value;
        }
    } else {
        return value;
    }
}

function horaParaMinutos(hora) {
    var hm = hora.split(':');
    return parseInt(hm[0]) * 60 + parseInt(hm[1]);
}

function dataHoraParaMinutos(dataHora) {
    return datahora.diff(datahora, 'minutes');
}

function converteMinutosParaHora(minutos) {
    var tempoMinutos = minutos % 60;
    var tempoHora = (minutos - tempoMinutos) / 60;
    return ("0" + tempoHora.toString()).slice(-2) + ":" + ("0" + tempoMinutos.toString()).slice(-2);
}

function formataHora(hora, minuto) {
    return ("0" + hora.toString()).slice(-2) + ":" + ("0" + minuto.toString()).slice(-2);
}

function formataMesAno(mes, ano) {
    return ("0" + mes).slice(-2) + "/" + ano;
}
