var app = angular.module('app', ['ng', 'ui.mask', 'ui.utils.masks', 'ngRoute', 'ngMaterial', 'ngMessages', 'diretivas', 'ngLoadingSpinner', 'angularUtils.directives.dirPagination', 'ui.utils.masks', 'ngCookies', 'angular-storage', 'ui.mask', 'asideModule', 'ui.sortable', '720kb.tooltips']);

app.controller('indexController2', function($rootScope, $scope, $cookies) {
    $scope.usuario = $cookies.getObject('usuario');
    $scope.version = $cookies.get('version');
    if ($scope.usuario){
        $scope.isAdmin = $scope.usuario.login.localeCompare('admin') === 0;
    }
});

app.config(function($routeProvider, $httpProvider, $mdThemingProvider) {
        $httpProvider.defaults.transformResponse.push(function(responseData) {
            convertDateStringsToDates(responseData);
            return responseData;
        });
        $routeProvider.when("/", {
            templateUrl: "view/home.html"
        }).when('/logout', {
            template: ' ',
            controller: function($scope, $location, store, $rootScope) {
                store.remove('jwt_token');
                $rootScope.loggedIn = false;
                window.location.replace('/plataformaWeb/login.html');
            }

        //CADASTROS
        }).when('/consultaSupermercado', {
            templateUrl: "view/consultaSupermercado.html",
            controller: "supermercadoController"
        }).when('/cadastrarSupermercado', {
            templateUrl: "view/supermercado.html",
            controller: "supermercadoController"
        }).when("/editarSupermercado/:id", {
            templateUrl: "view/supermercado.html",
            controller: "supermercadoController"

        }).when('/consultaCliente', {
            templateUrl: "view/consultaCliente.html",
            controller: "clienteController"
        }).when('/cadastrarCliente', {
            templateUrl: "view/cliente.html",
            controller: "clienteController"
        }).when("/editarCliente/:id", {
            templateUrl: "view/cliente.html",
            controller: "clienteController"

        }).when('/consultaProduto', {
            templateUrl: "view/consultaProduto.html",
            controller: "produtoController"
        }).when('/cadastrarProduto', {
            templateUrl: "view/produto.html",
            controller: "produtoController"
        }).when("/editarProduto/:id", {
            templateUrl: "view/produto.html",
            controller: "produtoController"

        }).when("/consultaFormaPagamento/", {
            templateUrl: "view/consultaFormaPagamento.html",
            controller: "formaPagamentoController"
        }).when("/cadastrarFormaPagamento", {
            templateUrl: "view/formaPagamento.html",
            controller: "formaPagamentoController"
        }).when("/editarFormaPagamento/:id", {
            templateUrl: "view/formaPagamento.html",
            controller: "formaPagamentoController"
        })
        //FIM CADASTRO

         //FATURAMENTO
         .when("/consultaOferta", {
            templateUrl: "view/consultaOferta.html",
            controller: "ofertaController"
        }).when("/editarOferta/:id", {
            templateUrl: "view/oferta.html",
            controller: "ofertaController"
        }).when("/cadastrarOferta", {
            templateUrl: "view/oferta.html",
            controller: "ofertaController"
        }).when("/consultaPedido", {
            templateUrl: "view/consultaPedido.html",
            controller: "pedidoController"
        }).when("/cadastrarPedido", {
            templateUrl: "view/pedido.html",
            controller: "pedidoController"
        }).when("/editarPedido/:id", {
            templateUrl: "view/pedido.html",
            controller: "pedidoController"
        })

        //RELATORIOS
        .when("/relatorioProdutoVendido", {
            templateUrl: "view/relatorio/relatorioProdutoVendido.html",
            controller: "relatorioProdutoVendidoController"
        }).when("/relatorioPedido", {
            templateUrl: "view/relatorio/relatorioPedido.html",
            controller: "relatorioPedidoController"
        });

        $httpProvider.interceptors.push('AuthenticationHttpInterceptor');
    }).run(function(store, $rootScope, $location, $route) {
        $rootScope.$on('$locationChangeStart', function(e, next, current) {
            if (!store.get('jwt_token')) {
                e.preventDefault();
                window.location.replace('/plataformaWeb/login.html');
            }
        });
    });
