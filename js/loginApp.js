var app = angular.module('loginApp', ['angular-storage', 'ngMaterial', 'ngCookies'])
    app.controller('loginController', function($scope, $rootScope, $http, store, $location, $cookies) {

        $scope.version = '0.0.0.1';
        $scope.usuario = {
            login: '',
            senha: ''
        };

        $scope.login = function() {
            $http.post('/sys/seguranca/login', $scope.usuario)
                .then(function(response) {
                    $scope.token = response.headers('Authorization');
                    store.set('jwt_token', response.headers('Authorization'));
                    window.location.replace('/plataformaWeb');
                    $cookies.putObject('usuario', $scope.usuario);
                    $cookies.put('version', $scope.version);
                }, function(response) {
                    if(response.status == 401){
                        $scope.feedback = 'Credenciais inválidas';
                    }else{
                        $scope.feedback = 'Erro no login';
                    }
                });
        };


    });
