app.service('AuthenticationHttpInterceptor', function(store, $rootScope) {
    this.request = function(config) {
        if (store.get('jwt_token')) {
            config.headers.Authorization = store.get('jwt_token');
            $rootScope.loggedIn = true;
        }
        return config;
    };
});
