app.service('mensagens', function() {
        this.showAlert = function(mensagem, tipo, titulo, fadeTimeout) {
            if (mensagem === undefined || mensagem.trim() === '') {
                console.log('service logpro.showAlert() error: a mensagem é obrigatória');
                return;
            }
            if (tipo === undefined || $.inArray(tipo, ['success', 'info', 'warning', 'danger']) == -1) {
                tipo = 'info';
            }
            var conteudo = '';
            if (titulo !== undefined && titulo.trim() !== '') {
                conteudo = '<strong>' + titulo.trim() + '</strong> ';
            }
            conteudo += mensagem;
            var alert = $('<div class="alert alert-' + tipo + ' fade">' +
                '  <a class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                conteudo +
                '</div>');
            $('#alertWrapper').append(alert);
            window.setTimeout(function() {
                alert.addClass("in");
            }, 0);
            if (fadeTimeout !== undefined) {
                window.setTimeout(function() {
                    alert.alert('close');
                }, fadeTimeout);
            }
        };
        this.showDataAlert = function(data, fadeTimeout) {
            if (!fadeTimeout){
                fadeTimeout = 5000;
            }
            if(data instanceof ArrayBuffer){
                var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
                data = JSON.parse(decodedString);
            }
            this.showAlert(data.mensagem, data.tipo, data.titulo, fadeTimeout);
        };
    });
