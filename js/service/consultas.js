app.service('consultas', function($mdDialog) {
        this.produto = function(ev, callback) {
            $mdDialog.show({
                controller: ProdutoController,
                templateUrl: '/plataformaWeb/view/consultas/produto.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: false
            }).then(callback);
        };
        this.cliente = function(ev, callback) {
            $mdDialog.show({
                controller: ClienteController,
                templateUrl: '/plataformaWeb/view/consultas/cliente.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: false
            }).then(callback);
        };
        this.oferta = function(ev, callback) {
            $mdDialog.show({
                controller: OfertaController,
                templateUrl: '/plataformaWeb/view/consultas/oferta.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: false
            }).then(callback);
        };
    });

    function ProdutoController($scope, $http, $mdDialog) {
        $scope.init = function () {
            $http.get("/sys/cadastro/produto")
                .then(function (response) {
                    $scope.listaProdutos = response.data;
                });
        };
        $scope.selecionar = function (produto) {
            $mdDialog.hide(produto);
        };
        $scope.cancel = function () {
            $mdDialog.hide(null);
        };
    }

    function ClienteController($scope, $http, $mdDialog) {
        $scope.init = function () {
            $http.get("/sys/cadastro/cliente")
                .then(function (response) {
                    $scope.listaClientes = response.data;
                });
        };
        $scope.selecionar = function (cliente) {
            $mdDialog.hide(cliente);
        };
        $scope.cancel = function () {
            $mdDialog.hide(null);
        };
    }

    function OfertaController($scope, $http, $mdDialog) {
        $scope.init = function () {
            $http.get("/sys/faturamento/oferta")
                .then(function (response) {
                    $scope.listaOfertas = response.data;
                });
        };
        $scope.selecionar = function (oferta) {
            $mdDialog.hide(oferta);
        };
        $scope.cancel = function () {
            $mdDialog.hide(null);
        };
    }

